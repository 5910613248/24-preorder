class Order < ApplicationRecord
    validates :name, presence: true
    validates :productname, presence: true
    validates :address, presence: true
end
