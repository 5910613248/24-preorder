class CreateMyUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :my_users do |t|
      t.string :email
      t.string :password_digest

      t.timestamps
    end
    add_index :my_users, :email, unique: true
  end
end
