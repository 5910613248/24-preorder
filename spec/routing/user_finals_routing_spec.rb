require "rails_helper"

RSpec.describe UserFinalsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/user_finals").to route_to("user_finals#index")
    end

    it "routes to #new" do
      expect(:get => "/user_finals/new").to route_to("user_finals#new")
    end

    it "routes to #show" do
      expect(:get => "/user_finals/1").to route_to("user_finals#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/user_finals/1/edit").to route_to("user_finals#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/user_finals").to route_to("user_finals#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/user_finals/1").to route_to("user_finals#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/user_finals/1").to route_to("user_finals#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/user_finals/1").to route_to("user_finals#destroy", :id => "1")
    end
  end
end
