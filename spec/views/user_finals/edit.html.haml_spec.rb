require 'rails_helper'

RSpec.describe "user_finals/edit", type: :view do
  before(:each) do
    @user_final = assign(:user_final, UserFinal.create!(
      :username => "MyString",
      :password => "",
      :name => "MyString",
      :surname => "MyString",
      :address => "MyString",
      :email => "MyString",
      :telephone => "MyString"
    ))
  end

  it "renders the edit user_final form" do
    render

    assert_select "form[action=?][method=?]", user_final_path(@user_final), "post" do

      assert_select "input[name=?]", "user_final[username]"

      assert_select "input[name=?]", "user_final[password]"

      assert_select "input[name=?]", "user_final[name]"

      assert_select "input[name=?]", "user_final[surname]"

      assert_select "input[name=?]", "user_final[address]"

      assert_select "input[name=?]", "user_final[email]"

      assert_select "input[name=?]", "user_final[telephone]"
    end
  end
end
