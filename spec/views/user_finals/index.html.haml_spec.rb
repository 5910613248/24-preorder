require 'rails_helper'

RSpec.describe "user_finals/index", type: :view do
  before(:each) do
    assign(:user_finals, [
      UserFinal.create!(
        :username => "Username",
        :password => "",
        :name => "Name",
        :surname => "Surname",
        :address => "Address",
        :email => "Email",
        :telephone => "Telephone"
      ),
      UserFinal.create!(
        :username => "Username",
        :password => "",
        :name => "Name",
        :surname => "Surname",
        :address => "Address",
        :email => "Email",
        :telephone => "Telephone"
      )
    ])
  end

  it "renders a list of user_finals" do
    render
    assert_select "tr>td", :text => "Username".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Surname".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Telephone".to_s, :count => 2
  end
end
