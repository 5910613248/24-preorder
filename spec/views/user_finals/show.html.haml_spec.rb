require 'rails_helper'

RSpec.describe "user_finals/show", type: :view do
  before(:each) do
    @user_final = assign(:user_final, UserFinal.create!(
      :username => "Username",
      :password => "",
      :name => "Name",
      :surname => "Surname",
      :address => "Address",
      :email => "Email",
      :telephone => "Telephone"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Username/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Surname/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Telephone/)
  end
end
